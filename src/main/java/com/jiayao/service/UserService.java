package com.jiayao.service;

import core.annotation.MyAutowired;
import core.annotation.MyComponent;

/**
 * 类 名: UserService
 * 描 述:
 * 作 者: 黄加耀
 * 创 建: 2019/12/5 : 23:55
 * 邮 箱: huangjy19940202@gmail.com
 *
 * @author: jiaYao
 */
@MyComponent
public class UserService {

    @MyAutowired
    private CustomerService myCust;

    public CustomerService getMyCust() {
        return myCust;
    }
}
