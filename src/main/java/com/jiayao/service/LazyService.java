package com.jiayao.service;

import core.annotation.MyComponent;
import core.annotation.MyLazy;

/**
 * 类 名: LazyService
 * 描 述:
 * 作 者: 黄加耀
 * 创 建: 2019/12/6 : 0:23
 * 邮 箱: huangjy19940202@gmail.com
 *
 * @author: jiaYao
 */
@MyLazy
@MyComponent
public class LazyService {

    public LazyService(){
        System.out.println("测试懒加载  LazyService 的无参构造被执行");
    }

}
