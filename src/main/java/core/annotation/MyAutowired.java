package core.annotation;

import java.lang.annotation.*;

/**
 * 实现Bean的属性填充，依赖注入
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
@MyComponent
public @interface MyAutowired {

}
