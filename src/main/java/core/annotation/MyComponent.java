package core.annotation;

import java.lang.annotation.*;

/**
 * 实现Bean的扫描，类加载时会添加到候选资源中，后期完成Bean的注入
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyComponent {

    String name() default "";
}
