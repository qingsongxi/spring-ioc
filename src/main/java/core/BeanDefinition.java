package core;

import lombok.Data;

/**
 * Bean定义，记录候选Bean的行为信息
 */
@Data
public class BeanDefinition {
    /**
     * Bean的id
     */
    private String beanName;
    /**
     * Bean的字节码对象
     */
    public Class<?> beanClass;
    /**
     * Bean的全限制名
     */
    public String beanReferenceName;
    /**
     * 是否是抽象的
     */
    private boolean abstractFlag = false;
    /**
     * 是否是懒加载的
     */
    private boolean lazyInit = false;
    /**
     * Bean的作用域 设置默认单例
     */
    private String scope = "singleton";

    public BeanDefinition(String beanName, Class<?> beanClass, String beanReferenceName) {
        this.beanName = beanName;
        this.beanClass = beanClass;
        this.beanReferenceName = beanReferenceName;
    }

    public BeanDefinition(String beanName, Class<?> beanClass, String beanReferenceName, boolean abstractFlag) {
        this.beanName = beanName;
        this.beanClass = beanClass;
        this.beanReferenceName = beanReferenceName;
        this.abstractFlag = abstractFlag;
    }

    public BeanDefinition(String beanName, Class<?> beanClass, String beanReferenceName, boolean lazyInit,  boolean abstractFlag) {
        this.beanName = beanName;
        this.beanClass = beanClass;
        this.beanReferenceName = beanReferenceName;
        this.lazyInit = lazyInit;
        this.abstractFlag = abstractFlag;
    }
}
