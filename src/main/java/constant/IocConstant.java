package constant;

/**
 * 类 名: IocConstant 常量类
 * @author: jiaYao
 */
public class IocConstant {
    /**
     * 模仿spring中的 多环境配置
     */
    public final static String active = "spring.profiles.active";
    /**
     * 模仿spring中的 引入配置
     */
    public final static String include = "spring.profiles.include";

}
