package factory;

import core.BeanDefinition;
import core.BeanPostProcessor;
import core.annotation.MyAutowired;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static scanner.ClassPathBeanDefinitionScanner.toLowercaseIndex;

/**
 * 类 名: DefaultListableBeanFactory
 * 描 述: 默认的Bean工厂
 * @author: jiaYao
 */
public class DefaultListableBeanFactory implements BeanFactory {

    /**
     * 一级缓存，用于存放完全初始化号的Bean，从该缓存中拿出来的Bean可以直接使用
     */
    private final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    /** Cache of early singleton objects: bean name --> bean instance */
    /**
     * 二级缓存，存储的原始的Bean(还未完全填充属性)，用于解决Spring中的循环依赖问题，此时已经分配了内存地址
     */
    private final Map<String, Object> earlySingletonObjects = new HashMap<>(16);

    /** Cache of singleton factories: bean name --> ObjectFactory */
    /**
     * 三级缓存，单例对象工厂的cache，存放 bean 工厂对象，用于解决循环依赖
     */
    private final Map<String, Object> singletonFactories = new HashMap<>(16);

    /**
     * Bean的源数据集合，存储的是扫描器扫描的类的源数据信息
     */
    public final Map<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(128);

    /**
     * 保存所有BeanPostProcessor
     */
    private Set<BeanPostProcessor> beanPostProcessorSet = new HashSet<>();
    /**
     * 配置文件
     */
    private Properties properties = null;

    /**
     * 动态向候选资源集合中添加候选数据
     *
     * @param beanName
     * @param beanDefinition
     */
    public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
        this.beanDefinitionMap.put(beanName, beanDefinition);
    }

    /**
     * 实例化所有单例非懒加载的bean
     */
    public void preInstantiateSingletons() {
        // 获取所有的候选资源Bean的名称
        Set<String> beanNames = beanDefinitionMap.keySet();
        beanNames.forEach(beanName -> {
            BeanDefinition bd = beanDefinitionMap.get(beanName);
            /**
             * 如果不是抽象的，并且是单例的，并且不是懒加载的Bean
             * 	抽象类不能被实例化，只能被继承
             */
            if (!Objects.isNull(bd) && !bd.isAbstractFlag() && !bd.isLazyInit()) {
                getBean(beanName);
            }
        });
    }

    @Override
    public Object getBean(String beanName) {
        return this.doGetBean(beanName);
    }

    @Override
    public Object doGetBean(String beanName) {
        if (singletonObjects.containsKey(beanName)) {
            return this.singletonObjects.get(beanName);
        }
        return this.createBean(beanName);
    }

    @Override
    public Object createBean(String beanName) {
        return this.doCreateBean(beanName);
    }

    @Override
    public Object doCreateBean(String beanName) {
        synchronized (this.getClass()) {
            // 先判断当前对象是否正在被创建
            if (earlySingletonObjects.containsKey(beanName) || singletonFactories.containsKey(beanName)) {
                throw new RuntimeException("IOC容器中存在循环依赖异常 beanName=" + beanName);
            }
            // 添加到三级缓存中，标记当前Bean正准备创建
            singletonFactories.put(beanName, "");
            BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
            if (Objects.isNull(beanDefinition)){
                return null;
            }
            Object beanObj = null;
            try {
                beanObj = beanDefinition.getBeanClass().newInstance();
                if (beanObj instanceof BeanPostProcessor) {
                    beanPostProcessorSet.add((BeanPostProcessor) beanObj);
                }
                // 添加到二级缓存中，标记当前Bean正在被创建，只是还没有完全填充属性
                earlySingletonObjects.put(beanName, beanObj);
                singletonFactories.remove(beanName);
                // 进行属性填充，属性填充后，有可能对象被反射之类的或者是产生了代理对象
                beanObj = populateBean(beanObj);

                // 进行Bean的后置处理器处理
                initializeBeanPostProcessor(beanName, beanObj);

                // 属性填充完毕后，保存当前对象到一级缓存中，表示当前Bean可以直接拿出来使用
                singletonObjects.put(beanName, beanObj);
                earlySingletonObjects.remove(beanName);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return beanObj;
        }


    }

    /**
     * 完成Bean的后置处理器工作
     * @param beanName
     * @param beanObj
     */
    private void initializeBeanPostProcessor(String beanName, Object beanObj) {
        for (BeanPostProcessor beanPostProcessor : beanPostProcessorSet) {
            beanObj = beanPostProcessor.postProcessBeforeInitialization(beanObj, beanName);
        }

        // 执行Bean的init方法
        System.out.println("假设正在执行" + beanName + "的init方法.....");

        for (BeanPostProcessor beanPostProcessor : beanPostProcessorSet) {
            beanObj = beanPostProcessor.postProcessAfterInitialization(beanObj, beanName);
        }
    }

    /**
     * 对当前Bean进行依赖注入
     *
     * @param beanObj 此处返回当前Bean，后期如果有更改，可能返回是一个代理类
     * @return
     */
    private Object populateBean(Object beanObj) throws IllegalAccessException {
        Field[] fields = beanObj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            // 判断当前属性是否需要进行依赖注入
            if (field.isAnnotationPresent(MyAutowired.class)) {
                String simpleName = toLowercaseIndex(field.getType().getSimpleName());
                // 先判断当前属性类型名称小写是否在候选资源中
                Set<String> beanDefinitionNames = beanDefinitionMap.keySet();
                if (!beanDefinitionNames.contains(simpleName)){
                    simpleName = toLowercaseIndex(field.getName());
                    if (!beanDefinitionNames.contains(simpleName)){
                        // 注入的属性的 类型名称小写 和 属性名称都不在候选资源中
                        throw new RuntimeException("当前类" + beanObj + "找不到属性类型" + field.getType() + "的Bean");
                    }
                }
                // 判断依赖的Bean是否已经被创建好可以直接被使用了
                if (singletonObjects.containsKey(simpleName)) {
                    field.set(beanObj, singletonObjects.get(simpleName));
                    // 查看二级缓存中是否存在
                } else if (earlySingletonObjects.containsKey(simpleName)) {
                    field.set(beanObj, earlySingletonObjects.get(simpleName));
                }else{
                    field.set(beanObj, getBean(simpleName));
                }
            }
        }
        return beanObj;
    }
    /**
     * 注册配置文件
     * @param properties
     */
    public void registerProperties(Properties properties) {
        this.properties = properties;
    }
}
