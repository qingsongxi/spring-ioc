package factory;

/**
 * 类 名: BeanFactory
 * 描 述:
 */
public interface BeanFactory {
   /**
    * 根据Bean的名称获取Bean对象
    */
   Object getBean(String beanName);

   /**
    * 真正开始获取Bean
    */
   Object doGetBean(String beanName);

   /**
    * 根据bean的名称创建Bean
    */
   Object createBean(String beanName);

   /**
    * 根据bean的名称真正开始创建bean
    */
   Object doCreateBean(String beanName);

}
