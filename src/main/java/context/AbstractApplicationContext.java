package context;

import core.BeanDefinition;
import factory.DefaultListableBeanFactory;
import registry.BeanDefinitionRegistry;

import java.util.Properties;
import java.util.Set;

/**
 * 类 名: AbstractApplicationContext
 * 描 述:
 * @author: jiaYao
 */
public class AbstractApplicationContext implements BeanDefinitionRegistry {
    /**
     * Bean工厂，用户Bean的创建和管理
     */
    public final DefaultListableBeanFactory beanFactory;

    /**
     * Create a new AbstractApplicationContext.
     */
    public AbstractApplicationContext() {
        /**
         * 调用父类的构造函数，为ApplicationContext Spring上下文对象初始化BeanFactory
         */
        this.beanFactory = new DefaultListableBeanFactory();
    }

    /**
     * 注入Bean
     */
    public void refresh(){
        // 获取bean工厂
        DefaultListableBeanFactory beanFactory = obtainFreshBeanFactory();
        // 工厂信息初始化完成后，开始创建单例非懒加载的bean
        finishBeanFactoryInitialization(beanFactory);
    }

    private void finishBeanFactoryInitialization(DefaultListableBeanFactory beanFactory) {
        // 开始进行Bean的注入工作
        beanFactory.preInstantiateSingletons();
    }


    /**
     * 关键 -> 往注册表中注册一个新的 BeanDefinition 实例
     */
    @Override
    public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
        this.beanFactory.registerBeanDefinition(beanName, beanDefinition);
    }

    /**
     * 从容器中获取Bean
     * @param beanName
     * @return
     */
    public Object getBean(String beanName){
        return this.beanFactory.getBean(beanName);
    }

    /**
     * 根据Bean的名称获取Bean的定义
     * @param beanName
     * @return
     */
    @Override
    public BeanDefinition getBeanDefinition(String beanName) {
        return this.beanFactory.beanDefinitionMap.get(beanName);
    }

    /**
     * 判断是否包含某个Bean
     * @param beanName
     * @return
     */
    @Override
    public boolean containsBeanDefinition(String beanName) {
        return this.beanFactory.beanDefinitionMap.containsKey(beanName);
    }

    /**
     * 获取所有Bean的id
     * @return
     */
    @Override
    public Set<String> getBeanDefinitionNames() {
        return this.beanFactory.beanDefinitionMap.keySet();
    }

    /**
     * 获取Bean工厂类
     * @return
     */
    private DefaultListableBeanFactory obtainFreshBeanFactory() {
        return this.beanFactory;
    }
    /**
     * 注册配置文件
     * @param properties
     */
    @Override
    public void registerProperties(Properties properties) {
        this.beanFactory.registerProperties(properties);
    }
}
